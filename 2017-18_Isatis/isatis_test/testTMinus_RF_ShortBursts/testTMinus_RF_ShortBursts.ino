
/* 
 *  This sketch emits numbered cycles of integers on the RF link.
 *  It is intended to be run on the T-Minus µController, with
 *  a RF board plugged in.
 *     
 *  If compiled for another board than the TMinus Serial1 is not used.
*/

#include "Wire.h"

const int RF_BaudRate = 19200;
const int blinkDelay  = 100; // msec on, then off. 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Init OK for serial link.");

#ifdef ARDUINO_TMinus
  Serial1.begin(RF_BaudRate);
  Serial.print("Baud rate for Serial1 UART:");
  Serial.println(RF_BaudRate);
  for (int led = LED_BUILTIN ; led <= LED_BUILTIN7; led++)
  {
    pinMode(led, OUTPUT);
    digitalWrite(led, HIGH);
  }
  while (!Serial1) ;
  Serial.println("Init OK for  RF transceiver.");
#else
  Serial << F("No TMinus card, test useless") << ENDL;
#endif

  Serial.println("End of Init");
  delay(100);
}


void loop() {
  // put your main code here, to run repeatedly:
  static  int i = 0;
  Serial.println(i);

#ifdef ARDUINO_TMinus
  while (Serial1.available()) {
    Serial1.read(); 
    // Just discard any possible incoming data.
  }
  Serial1.println(i);
  for (int led = LED_BUILTIN ; led <= LED_BUILTIN7; led++)
  {
    while (Serial1.available()) 
    {
      Serial1.read(); 
      // Just discard any possible incoming data.
    }
    Serial.println(led);
    Serial1.print(" RF ");
    Serial1.println(led);
    digitalWrite(led, LOW);
    while (Serial1.available()) 
    {
      Serial1.read(); 
      // Just discard any possible incoming data.
    }
    delay(blinkDelay);
    digitalWrite(led, HIGH);
    while (Serial1.available()) 
    {
      Serial1.read(); 
      // Just discard any possible incoming data.
    }
    delay(blinkDelay);
  }
#else
  delay(1000);
#endif
  i++;
}
