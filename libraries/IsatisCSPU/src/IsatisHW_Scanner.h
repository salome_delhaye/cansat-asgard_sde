/*
 * IsatisHWScanner.h
 *
 *  Created on: 26 janv. 2018
 *
 */

#pragma once
#include "HardwareScanner.h"

class IsatisHW_Scanner: public HardwareScanner {
  public:
    typedef struct Flags {
      unsigned int SD_CardReaderAvailable:1;
      unsigned int BMP_SensorAvailable:1;
      unsigned int ADT_SensorAvailable:1;
      // NB: no flag for the BPY sensor: we cannot possibly detect it. 
    } Flags;
    
    IsatisHW_Scanner(const byte unusedAnalogInPin=0);
    virtual ~IsatisHW_Scanner() {};
    virtual void IsatisInit();
    virtual void checkAllSPI_Devices();
    virtual void printSPI_Diagnostic(Stream& stream) const;
    virtual void printI2C_Diagnostic(Stream& stream) const;
    virtual byte getLED_PinNbr(LED_Type type);
    const Flags& getFlags() const { return flags;};
#ifdef IGNORE_EEPROMS  // This is just used to avoid consuming write cycles during tests.
    virtual byte getNumExternalEEPROM() const;
#endif

  private:
    Flags flags;  //  The flags to indicate the availability of the various devices. 
    
};
