/*
 * test_IMU_Client.ino
 * 
 * Test file for class IMU_Client.
 */
 
#include "DebugCSPU.h"
#include "IsaTwoRecord.h"
#include "IMU_Client.h"
#include "Wire.h"
#include "elapsedMillis.h"

IMU_Client imu;
IsaTwoRecord record;
elapsedMillis elapsed;

void setup() {
  DINIT(115200);
  Wire.begin();
  imu.begin();
}

void loop() {
  elapsed=0;
  imu.readData(record);
  Serial << "Took a reading in " << elapsed << " msecs" << ENDL;
  record.print(Serial, record.DataSelector::IMU);
  delay(1000);
}
