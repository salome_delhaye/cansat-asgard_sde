/*
    test_IsaTwoRecord.ino

    Test program for class IsaTwoRecord.
*/
#include "IsaTwoRecord.h"
#define DEBUG
#include "DebugCSPU.h"

#define TEST_HUMAN_READABLE_PRINT  // undefine to avoid testing non operational code. 

unsigned int numErrors = 0;

void requestVisualCheck() {
  char answer = ' ';
  Serial << ENDL;
  // remove any previous character in input queue
  while (Serial.available() > 0) {
    Serial.read();
  }

  while ((answer != 'y') && (answer != 'n')) {
    Serial << "Is this ok (y/n) ? ";
    Serial.flush();
    // remove any previous character in input queue
    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  if (answer == 'n') {
    numErrors++;
  }
  Serial << ENDL;
}

class IsaTwoRecord_Test {
  public:
    IsaTwoRecord_Test(IsaTwoRecord& theRecord) : record(theRecord) {}
    void testIndividualPrints() {
      float testFloatArr[3] = {1.11, 2.22, 3.33};
      int16_t testIntArr[3] = {4, 5, 6};
      Serial << "Printing CSV Array values with NO final separator" << ENDL;
      record.printCSV(Serial, testFloatArr);
      requestVisualCheck();
      record.printCSV(Serial, testIntArr);
      requestVisualCheck();
      Serial << "Printing CSV Array values WITH final separator" << ENDL;
      record.printCSV(Serial, testFloatArr, true);
      requestVisualCheck();
      record.printCSV(Serial, testIntArr, true);
      requestVisualCheck();
      Serial << "Printing a CSV Float with NO final separator" << ENDL;
      record.printCSV(Serial, (float) 12.3);
      requestVisualCheck();
      Serial << "Printing a CSV Value With a final separator" << ENDL;
      record.printCSV(Serial, (float) 12.3, true);
      requestVisualCheck();
    }
    IsaTwoRecord& record;
} ;

IsaTwoRecord record;

void assignValues (IsaTwoRecord& rec) {
  rec.timestamp = 1;
  rec.accelRaw[0] = 2;
  rec.accelRaw[1] = 3;
  rec.accelRaw[2] = 4;
  rec.accel[0] = 5.5;
  rec.accel[1] = 6.6;
  rec.accel[2] = 7.7;
  rec.gyroRaw [0] = 8;
  rec.gyroRaw [1] = 9;
  rec.gyroRaw [2] = 10;
  rec.gyro[0] = 11.11;
  rec.gyro[1] = 12.12;
  rec.gyro[2] = 13.13;
  rec.mag[0] = 20.20;
  rec.mag[1] = 21.21;
  rec.mag[2] = 22.22;
  
  rec.newGPS_Measures = true;
  rec.GPS_LatitudeDegrees = 25.25;
  rec.GPS_LongitudeDegrees = 26.26;
  rec.GPS_Altitude = 27.27;
  rec.GPS_VelocityKnots = 28.28;
  rec.GPS_VelocityAngleDegrees = 29.29;
#ifdef INCLUDE_AHRS_DATA
  rec.AHRS_Accel[0] = 30.30;
  rec.AHRS_Accel[1] = 31.31;
  rec.AHRS_Accel[2] = 32.32;
  rec.roll = 33.33;
  rec.yaw = 34.34;
  rec.pitch = 35.35;
#endif
  
  rec.temperature = 36.36;
  rec.pressure = 37.37;
  rec.altitude = 38.38;
  rec.data1= 39.39;
  rec.data2= 40.40;
}

void performTest() {
  assignValues(record);
  IsaTwoRecord_Test friendClass(record);
  friendClass.testIndividualPrints();

  Serial << "Values Assigned (printing complete record in CSV format (with header):" << ENDL << "   ";
  record.printCSV_Header(Serial);
  Serial << ENDL << "   ";
  record.printCSV(Serial);
  requestVisualCheck();

  Serial << ENDL << "Testing IsaTwoRecord Clear(): " << ENDL << "   ";
  record.clear();
  record.printCSV(Serial);
  Serial << ENDL;
  DASSERT(rec.IMU_AccelRaw[0] == 0); DASSERT(rec.IMU_AccelRaw[1] == 0); DASSERT(rec.IMU_AccelRaw[2] == 0);
  DASSERT(rec.IMU_Accel[0] == 0); DASSERT(rec.IMU_Accel[1] == 0); DASSERT(rec.IMU_Accel[2] == 0);
  DASSERT(rec.gyroRaw[0] == 0); DASSERT(rec.gyroRaw[1] == 0); DASSERT(rec.gyroRaw[2] == 0);
  DASSERT(rec.magRaw[0] == 0); DASSERT(rec.magRaw[1] == 0); DASSERT(rec.magRaw[2] == 0);
  DASSERT(rec.gyro[0] == 0); DASSERT(rec.gyro[1] == 0); DASSERT(rec.gyro[2] == 0);
  DASSERT(rec.mag[0] == 0); DASSERT(rec.mag[1] == 0); DASSERT(rec.mag[2] == 0);
  Serial << "   Values checked. " << ENDL;

#ifdef TEST_HUMAN_READABLE_PRINT
  assignValues(record);
  Serial << "Printing complete record in Human Readable Format:" << ENDL << "   ";
  record.print(Serial);
  Serial << ENDL;
  requestVisualCheck();
#endif
  Serial << "End of job. Total number of errors:" << numErrors <<  ENDL;
  Serial << "Size of IsaTwoRecord is " << sizeof(IsaTwoRecord) << " bytes." ;
}

void setup() {
  Serial.begin(115200);
  assignValues(record);
  performTest();
}

void loop() {}
