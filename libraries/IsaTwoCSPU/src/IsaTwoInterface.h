/*
 * IsaTwoInterface.h
 * This file contains definitions shared by the CanSat, and the ground systems
 */

#pragma once 

/**  @ingroup IsaTwoCSPU
 *  Constants used to manage the type of CSV strings exchanged between the CanSat and 
 *  the ground systems.
 */
 enum class IsaTwoRecordType { 
                DataRecord=0, /**< Value denoting a measurement record */
                CmdRequest=1, /**< Value denoting a command (request)  */
                CmdResponse=2, /**< Value denoting a command response   */
                GroundDataRecord=100  /**< Value denoting a ground record */
 }; 

 /**  @ingroup IsaTwoCSPU
  *   Constants used to identify command requests */
 enum class IsaTwoCmdRequestType {
                InitiateCmdMode=0,
                TerminateCmdMode=1,
                // To complete
 };

 /**  @ingroup IsaTwoCSPU
  *   Constants used to identify command responses */
 enum class IsaTwoCmdResponseType {
                CmdModeInitiated=0,
                CmdModeTerminated=1,
                // To complete
 };
