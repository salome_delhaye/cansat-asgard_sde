/*
 * DoubleSerial.h
 * 
 */
 
#pragma once

// Temporary: this class currently only tested with Genuino Uno and does not work
// with SAMD board because their core does not include the SoftwareSerial library.
// Solution: Configure an additional serial port using a SerCom (implemented by 
// Serial2. h
#ifndef ARDUINO_SAMD_ZERO
#include <SoftwareSerial.h>
#include <Arduino.h>

/** @ingroup cansatAsgardCSPU
 *  @brief This class splits a data flow over two different serial ports.
 *  Routing is performed line by line, as follows:
 *    - If the first characters of the line are a well-formed integer, followed
 *      by a comma, this number is compared to a selection code.
 *      If equal to the selection code, the line is forwarded to the second serial port
 *      else it is forwarded to the default Serial object.
 *    - Else the line is forwarded to the default Serial object.
 *  
 *  @warning The second serial port is currently managed using SoftwareSerial. If 
 *  using a board with additional hardware serial controllers, consider improving
 *  this class to use the hardware controller if any available. Possible solution: 
 *    - Turn this class into a superclass that uses a fully initialized second serial 
 *      object and includes the routing logic.
 *    - Create a subclass SoftwareDoubleSerial, with the same constructor as the 
 *      the current DoubleSerial, which would only create the SoftwareSerial object
 *      for use by the superclass.
 */
class DoubleSerial {
  public:
  /** @brief The constructor only stores the configuration data. 
   *  Actual initialization happens when begin() is called.
   *  
   *  @param rxPin The number of the digital pin used for reception (connected to the tx of the second device).
   *  @param txPin The number of the digital pin used for transmission(connected to the rx of the second device).
   *  @param selectionCode The number used to select the port to send the data
   *         to. See class documentation for the routing logic.
   * 
   */
    DoubleSerial(const byte rxPin, const byte txPin, const uint16_t selectionCode);
    /**
     * @brief Initialization of the object (call before any use).
     * @param baudRate The baud rate to use on the second serial port.
     * @pre The standard Serial object is assumed to be preperly initiailized.
     */
     void begin(const uint32_t baudRate);
     
     /**
      * @brief Stream the data to one of the serial ports
      * See class documentation for the routing logic.
      * @param str The string to process..
      */
    DoubleSerial& operator<<(const char * str);
    
    /** @brief This fonction is used to print the data possibly buffered since 
     *  the last calls to the streaming operator. */
    void flush();

  private:
    uint16_t selectionCode; /*!< The value triggering the routing to the second serial port. */
    SoftwareSerial serial2;  /*!< The object managing the second serial port. */
};

#endif
