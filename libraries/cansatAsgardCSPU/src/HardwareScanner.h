/* 
 *  HardwareScanner.h
*/

#ifndef __HARDWARE_SCANNER_H__
#define __HARDWARE_SCANNER_H__

#pragma once
#include "Arduino.h"
#include "VirtualMethods.h"

/** @ingroup cansatAsgardCSPU
 *  @brief A generic helper class to check whether required HW is indeed present
   and responsive.

   Scans the I2C bus, and can be subclassed to check for SPI slaves (implement
   scanForSPI_Devices() method.
   The following assumptions are made. Use a subclass to work on different assuptions.
   - Addresses 0x50 to 0x57 are assumed to be I2C EEPROMs
   - All I2C EEPROMs are assumed to be the same size (provided to constructor).

   Size of the object: about 20 to 30 bytes (depends on the board used. 

   @remark Currently supports the AVR board and SAMD board. 
   - For SAMD board, the HardwareScanner
     includes "Serial2.h" which causes the sketch to configure Sercom1 for use as Serial2 port.
     Would the program use SerCom1 for any other purpose, this would create a conflict.  
   - For SAMD boards, the I2C delay seems to be must longer than on Uno: takes about 0.5 sec to explore
     and I2C address, which makes the scanner quite slow. On Uno board, it's much much faster... 
   
   Possible optimizations:
   - Store port number of RF_Serial, rather than pointer to Serial (save 1 byte). But how to find the
     serial object just from the number?
   - Pack I2C addresses?  7 bits used on the 8. Addresses 0 to 7 are reserved so 6 bits are really necessary.
     Use 128 1 bit flags, i.e. 16 bytes for all possible addresses? This increases the size but could
     be combined with a template definition allowing to have an class supporting 8n addresses in a predefined
     range, using n+1 byte (1 for the first address).
 */
class HardwareScanner
{
  public:
    typedef enum LED_Type  {
      Init, Storage, Transmission, Acquisition, Heartbeat, Campaign, UsingEEPROM
    } LED_Type;
    static const unsigned int maxNbrOfI2C_Slaves = 10;
    /** @param unusedAnalogInPin The number of an unused analog input pin. It is used
     *         to read a random seed.
     */
    HardwareScanner(const byte unusedAnalogInPin=0);
    VIRTUAL ~HardwareScanner() {};
    /** Call once, before using, once the setup() function of the sketch has been entered
       (to ensure all board functions are up and running)
       Note that this call takes care of calling Wire.begin(), and setting the clock rate to 400kHz
       @param firstI2C_Address  First I2C address to consider.
       @param lastI2C_Address   Last I2C address to consider.
       @param RF_SerialPort The ID of the serial port to use for the RF transmitter (0 = none)
       @param bitRateInKhz  bitRate to use to initialize the Wire library.
    */
    VIRTUAL_FOR_TEST void init(  const byte firstI2C_Address = 1,
                        const byte lastI2C_Address = 127,
                        const byte RF_SerialPort = 0,
                        const uint16_t bitRateInKhz = (uint16_t) 400);

    /* ----- Diagnostic methods ------------ */
    NON_VIRTUAL void printFullDiagnostic(Stream& stream) const;
    /**  If expanding the printI2C_Diagnostic, the subclass should call the inherited method before generating 
          any additional diagnostic.
         @param stream The stream to send diagnostic to.
    */
    VIRTUAL void printI2C_Diagnostic(Stream& stream) const;
    NON_VIRTUAL void printSerialPortsDiagnostic(Stream& stream) const;
    /** Implement this method in subclass if any SPI device is used. The subclass should also provide its 
     *  own methods to check whether the SPI slaves are up and running.
     *  @param stream The stream to send diagnostic to.
     */
    virtual void printSPI_Diagnostic(Stream& stream) const;

    // ----- HW interrogation methods ------
    // First group should usually not need any implementation in subclasses.
    NON_VIRTUAL bool isI2C_SlaveUpAndRunning(const byte slaveAddress) const;
    NON_VIRTUAL bool isSerialPortAvailable(const byte portNbr) const;
    NON_VIRTUAL int  getInternalEEPROM_LastAddress() const {
#ifdef __SAMD21G18A__
      // This µC has no EEPROM. 
      return 0;
#else
      return E2END;
#endif
    }
    VIRTUAL byte getNumExternalEEPROM() const;
    NON_VIRTUAL HardwareSerial* getRF_SerialObject() const;
    /* Scan the I2C bus and store results for access through isI2C_SlaveUpAndRunning() */
    NON_VIRTUAL void scanI2C_Bus( const byte firstAddress = 1, const byte lastAddress = 120,
                              const byte bitRateInKhz = (byte) 400);
    /* The following two methods should never be called if getNumExternalEEPROM() returns 0.
       Practically, any system using external EEPROM should subclass HardwareScanner,
       Implement getNumExternalEEPROM(), getExternalEEPROM_Address() and getExternalEEPROM_Size().
       The EEPROM_Number should be >=0 and < getNumExternalEEPROM().
       WARNING: Those 2 methods rely on assuptions described in the header of this file.
                Overwrite them in subclass if these assumptions are not applicable to you.
    */
    VIRTUAL_FOR_TEST byte getExternalEEPROM_I2C_Address(const byte EEPROM_Number) const ;
    VIRTUAL unsigned int getExternalEEPROM_LastAddress(const byte EEPROM_Number) const ;
    // This method by default always returns LED_BUILTIN.
    // Redefine in subclass if you have more than one LED available. Return 0 if
    // no LED is available for a particular function.
    VIRTUAL byte getLED_PinNbr(LED_Type type);

    /* Implement this method to check for your SPI devices one by one */
    VIRTUAL void checkAllSPI_Devices() {};
  protected:
    /* Return true if the chip is 64 kbytes, false otherwise (it is then assumed to be 32 kBytes) */
    NON_VIRTUAL bool check64Kbyte(const byte I2C_Address) const;
    byte I2C_Adresses[maxNbrOfI2C_Slaves];
    byte nbrOfI2C_Slaves;
    unsigned int I2C_EEPROM_LastAddress;
    byte I2C_EEPROM_Flags; // each byte denotes EEPROM à address 0x50+bit order.
    HardwareSerial *RF_Serial;
} ;

#endif
