/*
    AcquisitionProcess.h
    A class implementing all the logic common to any acquisition process:
    read data from sensors, and store using a provided StorageManager

    Created on: 19 janv. 2018

*/

#pragma once
#include "StorageManager.h"

#include "elapsedMillis.h"

/** @ingroup cansatAsgardCSPU 
 *  @brief A COMPLETER
 *  
 *  A COMPLETER
 *  
 */
class AcquisitionProcess {
  public:
    /* Constants for switching element on or off */
    static const bool On = true;
    static const bool Off = false;

    /* Constructor/Destructor */
    AcquisitionProcess(unsigned int acquisitionPeriodInMsec);

    /* This is the method to call in the main loop() to have acquisition cycles performed with the period defined
       by the last call to setMinDelayBetweenAcquisition() */
    NON_VIRTUAL void run();

    /* Call once and only once before calling run(). getHardwareScanner() is expected to return an
       initialized HardwareScanner when this method is called. */
    VIRTUAL void init();

    /* Implement in subclass to make the appropriate HardwareScanner available to the process */
    VIRTUAL HardwareScanner* getHardwareScanner() = 0;

  protected:
    VIRTUAL void storeDataRecord(const bool campaignStarted) = 0;
    VIRTUAL void doIdle()=0;
     /* Switch on/off the LED for init. Valid values for status are On and Off*/
    NON_VIRTUAL void setLED(HardwareScanner::LED_Type type, bool status);

  private:
    /* Abstract: actually acquire the data from the sensors */
    VIRTUAL void acquireDataRecord() = 0;

    /* Define whether this data set is worth storing. This default implementation always returns true */
    VIRTUAL bool isRecordRelevant() const;

    /* Define whether the measurement campain is started
       This default implementation always returns true but should be overloaded in the subclasses to define a real condition to detect
       when the payload actually measures relevant data. The most recent data record can be accessed using the getData() method.  */
    VIRTUAL bool measurementCampaignStarted() {
      return true;
    } ;
    NON_VIRTUAL void initLED_Hardware();
    virtual void initSpecificProject() {};

    unsigned int periodInMsec;
    elapsedMillis elapsedTime; // The time elapsed since the previous cycle in milliseconds.
    void blinkHeartbeatLED ();
    elapsedMillis heartbeatTimer;
    bool heartbeatLED_State;



};
