/*
   Test program for HardwareScanner class
*/
#include "HardwareScanner.h"
#include "HW_Subclass.h"
#define DEBUG
#include "DebugCSPU.h"

void checkAvailabilityOfSerialPorts(const HardwareScanner& hw) {
  Serial.println("");
  Serial.print("Checking availability of serial ports: ");
  for (int i = 0; i < 4; i++) {
    Serial.print(i);
    Serial.print(F(":"));
    Serial.print(hw.isSerialPortAvailable(i));
    Serial.print(F(" "));
  }
}

void testHW_ScannerWithoutRF_Port() {
  Serial.println("*** Testing HardwareScanner without RF port....");
  Serial.println("Full diagnostic:");
  HardwareScanner hw;
  hw.init(10, 100); 
  hw.printFullDiagnostic(Serial);
  checkAvailabilityOfSerialPorts(hw);
  Serial.println("");

  DASSERT(hw.getNumExternalEEPROM() == 0);
  DASSERT(hw.getExternalEEPROM_Address(25) == 0);
  DASSERT(hw.getExternalEEPROM_Size(25) == 0);
  DASSERT(hw.getRF_SerialObject() == NULL);
  DASSERT(hw.getPinNbr_TransmissionLED() == LED_BUILTIN);
  DASSERT(hw.getPinNbr_StorageLED() == LED_BUILTIN);
  DASSERT(hw.getPinNbr_AcquisitionLED() == LED_BUILTIN);
  DASSERT(hw.getPinNbr_HartbeatLED() == LED_BUILTIN);
  DASSERT(hw.getPinNbr_InitLED() == LED_BUILTIN);
  Serial.println("");
}

void testHW_ScannerWithRF_Port() {
  {
    Serial.println("*** Testing HardwareScanner with existing RF port....");
    HardwareScanner hw;
    hw.init(10, 100, 1);
    Serial.println("Full diagnostic:");
    hw.printFullDiagnostic(Serial);
    checkAvailabilityOfSerialPorts(hw);
  }
  Serial.println("");

  {

    Serial.println("*** Testing HardwareScanner with unexistant RF port....");
    HardwareScanner hw;
    hw.init(10, 100, 2);
    Serial.println("Full diagnostic:");
    hw.printFullDiagnostic(Serial);
    checkAvailabilityOfSerialPorts(hw);
  }
  Serial.println("");
}


void testSubclass() {

  Serial.println("*** Testing HardwareScanner subclass with 3 EEPROMS, 32k+16k+8k....");
#ifdef USING_SIMULATED_ARDUINO
  Serial.println("Second EEPROM chip should be reported as out-of-order");
#endif
  HW_Subclass hw;
  hw.init(10, 100);
  Serial.println("Full diagnostic:");
  hw.printFullDiagnostic(Serial);
  checkAvailabilityOfSerialPorts(hw);
  Serial.println("");
  DASSERT(hw.getPinNbr_TransmissionLED() == LED_BUILTIN);
  DASSERT(hw.getPinNbr_StorageLED() == 2);
  DASSERT(hw.getPinNbr_AcquisitionLED() == 5);
  DASSERT(hw.getPinNbr_HartbeatLED() == LED_BUILTIN);
  DASSERT(hw.getPinNbr_InitLED() == LED_BUILTIN);
}

void testHardwareScanner() {

  Serial.println("*** Testing HardwareScanner class with real hardware....");
  HardwareScanner hw;
  hw.init();
  Serial.println("Full diagnostic:");
  hw.printFullDiagnostic(Serial);
  checkAvailabilityOfSerialPorts(hw);
  Serial.println("");
}
//************************************************************

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
#ifdef USING_SIMULATED_ARDUINO

  Serial.println("Testing with simulated Arduino environment");

  testHW_ScannerWithoutRF_Port();
  testHW_ScannerWithRF_Port();
#else
  testHardwareScanner();
#endif
  testSubclass();
  Serial << F("Size of HardwareScanner class: ") << sizeof(HardwareScanner) << F(" bytes") << ENDL;
  Serial.println("*** End of test ***");

}

void loop() {
  // put your main code here, to run repeatedly:

}
